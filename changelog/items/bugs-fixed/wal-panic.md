- Fix panic in the wal of the siadir and siafile if a delete update was
  submitted as the last update in a set of updates.

- Add `disabledefaultpath` - a new optional path parameter when creating 
Skylinks. It disables the default path functionality, guaranteeing that the user
will not be automatically redirected to `/index.html` if it exists in the 
skyfile.

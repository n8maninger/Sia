- Optimise writes when we execute an MDM program on the host to lower overall
  (upload) bandwidth consumption.
